const routes = require("next-routes")();

routes
  .add("/campaigns/new", "/campaigns/new")
  .add("/campaigns/0x:address", "/campaigns/show")
  .add("/campaigns/0x:address/requests", "/campaigns/requests/index")
  .add("/campaigns/0x:address/requests/new", "/campaigns/requests/new");

module.exports = routes;
