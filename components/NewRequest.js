import React, { Component } from "react";
import { Form, Button, Message, Input } from "semantic-ui-react";

class NewRequest extends Component {
  state = {
    value: "",
    description: "",
    recipient: "",
  };

  submitForm = (event) => {
    event.preventDefault();
    this.props.onSubmit(this.state);
  };

  render() {
    const state = this.state;
    return (
      <Form onSubmit={this.submitForm} error={!!this.props.error}>
        <Form.Field>
          <label>Description</label>
          <Input
            value={this.state.description}
            onChange={(event) =>
              this.setState({ description: event.target.value })
            }
          />
        </Form.Field>
        <Form.Field>
          <label>Value in Ether</label>
          <Input
            value={this.state.value}
            onChange={(event) => this.setState({ value: event.target.value })}
          />
        </Form.Field>
        <Form.Field>
          <label>Recipient</label>
          <Input
            value={this.state.recipient}
            onChange={(event) =>
              this.setState({ recipient: event.target.value })
            }
          />
        </Form.Field>
        <Message error header="Oops!" content={this.props.error} />

        <Button loading={this.props.loading} primary>
          Create!
        </Button>
      </Form>
    );
  }
}

export default NewRequest;
