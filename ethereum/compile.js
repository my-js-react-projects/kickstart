const path = require("path");
const solc = require("solc");
const fs = require("fs-extra");

// delete any pre vious build
const buildPath = path.resolve(__dirname, "build");
fs.removeSync(buildPath);

// Read the Campain contract source code
const campaignPath = path.resolve(__dirname, "contracts", "Campaign.sol");
const source = fs.readFileSync(campaignPath, "utf-8");

// compile the source code
const output = solc.compile(source, 1).contracts;
// the output contains a object that contains the contracts
// recreate the build directory to hold the build from the output
fs.ensureDirSync(buildPath);

for (let contract in output) {
  fs.outputJsonSync(
    path.resolve(buildPath, contract.replace(":", "") + ".json"),
    output[contract]
  );
}
