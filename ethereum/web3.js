const Web3 = require("web3");

let web3;

if (typeof window !== "undefined" && typeof window.ethereum !== "undefined") {
  // We are in a browser and metamask is running
  window.ethereum.request({ method: "eth_requestAccounts" });
  web3 = new Web3(window.ethereum);
} else {
  // We are on the server "OR" the user is not running metamask
  const provider = new Web3.providers.HttpProvider(
    // so we use the Api endpoint from rinkeby as provider
    "https://rinkeby.infura.io/v3/303e3169da1941baa205519a89198061"
  );
  web3 = new Web3(provider);
}

export default web3;
