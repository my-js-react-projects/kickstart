import web3 from "./web3";

const CampaignFactory = require("./build/CampaignFactory.json");

const factory = new web3.eth.Contract(
  JSON.parse(CampaignFactory.interface),
  "0x61592Fa5E2b10F32C0bD2E96dAdFA4A034c6BEcA"
);

export default factory;
