import React, { Component } from "react";
import { Message, Button } from "semantic-ui-react";
import Layout from "../../../components/Layout";
import NewRequest from "../../../components/NewRequest";
import Campaign from "../../../ethereum/campaign";
import web3 from "../../../ethereum/web3";
import { Link, Router } from "../../../routes";

class Request extends Component {
  state = {
    loading: false,
    errorMessage: "",
  };

  static async getInitialProps(props) {
    const { address } = props.query;

    return { address };
  }

  onSubmit = async (state) => {
    this.setState({ loading: true, errorMessage: "" });

    const { description, value, recipient } = state;
    const campaign = Campaign(this.props.address);

    try {
      const accounts = await web3.eth.getAccounts();
      await campaign.methods
        .createRequest(description, web3.utils.toWei(value, "ether"), recipient)
        .send({
          from: accounts[0],
        });

      Router.pushRoute(`/campaigns/0x${this.props.address}/requests`);
    } catch (err) {
      this.setState({ errorMessage: err.message, loading: false });
    }
    this.setState({ loading: false });
  };

  render() {
    return (
      <Layout>
        <Link route={`/campaigns/${this.props.address}/requests`}>
          <a>Back</a>
        </Link>
        <h3>Create a Request</h3>
        <NewRequest
          error={this.state.errorMessage}
          loading={this.state.loading}
          onSubmit={this.onSubmit}
        />
      </Layout>
    );
  }
}

export default Request;
