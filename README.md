# kickstart

- This is my solution to one of the projects in a course:
- [Ethereum and Solidity: The Complete Developer's Guide](https://www.udemy.com/course/ethereum-and-solidity-the-complete-developers-guide/)

## About the projects

- The project is about using smart contracts to try to solve a famous problem experienced by kickstarter

- When you create a project or campaign and ask people to donate to the project, there needs to be a way to effeciently manage the campaign and how the donated money is spent in order to realise the objective.

### How does this solution work?

- After a campaign is created, the creator of the campaign is assigned the manager role
- Managers create a spending request
- The manager has the responsibility in creating a spending request
- All the people who are contributors to the campaign will have to approve/disapprove the spending request for it to be approved
- The request when approved will have funds sent to a vendor who is supplying a component part in building the project
- If too many people vote 'NO' that means this is a bad request

- This solution goes a long way to give power to the contributors to a project on how their money is spent and realizing the goal.

## Authors and acknowledgment

Benjamin Major under the tutorship of Stephen Grider.
